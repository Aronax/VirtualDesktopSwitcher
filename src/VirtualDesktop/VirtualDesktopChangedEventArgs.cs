﻿using System;

namespace WindowsDesktop
{
    /// <summary>
    ///     Provides data for the <see cref="VirtualDesktop.CurrentChanged" /> event.
    /// </summary>
    public class VirtualDesktopChangedEventArgs : EventArgs
    {
        public VirtualDesktopChangedEventArgs(VirtualDesktop oldDesktop, VirtualDesktop newDesktop)
        {
            OldDesktop = oldDesktop;
            NewDesktop = newDesktop;
        }

        public VirtualDesktop OldDesktop { get; }
        public VirtualDesktop NewDesktop { get; }
    }
}