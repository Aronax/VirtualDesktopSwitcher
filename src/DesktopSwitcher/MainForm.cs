﻿using System;
using System.Windows.Forms;

namespace VDSwitch
{
    public partial class MainForm : Form
    {
        private ApplicationContext _context;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;

            _context = new ApplicationContext();
            bindingSource1.DataSource = _context.Settings;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                ShowInTaskbar = false;
                notifyIcon1.Visible = true;
                Hide();
            }
            else if (FormWindowState.Normal == WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_allowClose)
            {
                WindowState = FormWindowState.Minimized;
                e.Cancel = true;
            }
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left &&
                WindowState == FormWindowState.Minimized)
            {
                ShowInTaskbar = true;
                Show();
                WindowState = FormWindowState.Normal;
            }
        }

        private bool _allowClose = false;
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _allowClose = true;
            Close();
        }
    }
}