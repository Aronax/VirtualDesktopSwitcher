﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsDesktop;
using DesktopSwitcher.Annotations;
using Microsoft.Win32;
using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;

namespace VDSwitch
{
    internal class ApplicationContext
    {
        private readonly int _delay = 80;
        private readonly KeyboardHookListener _keyboardHookManager;
        private readonly MouseHookListener _mouseHookManager;

        private volatile bool _switchInProgress;
        
        public ApplicationContext()
        {
            _keyboardHookManager = new KeyboardHookListener(new GlobalHooker()) {Enabled = true};
            _mouseHookManager = new MouseHookListener(new GlobalHooker()) {Enabled = true};

            _mouseHookManager.MouseWheelHorizontal += MouseWheelHorizontal;
            _keyboardHookManager.KeyDown += KeyDown;
        }

        public ApplicationSettings Settings { get; set; } = new ApplicationSettings();

        private void KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.CapsLock || e.KeyCode == Keys.F15)
            {
                if (e.Shift)
                {
                    if (Settings.ShiftCapsLock)
                    {
                        Switch(Direction.Left, Settings.ShiftCapsLockRound);
                    }
                }
                else
                {
                    if (Settings.CapsLock)
                    {
                        Switch(Direction.Right, Settings.CapsLockRound);
                    }
                }
            }
        }

        private void MouseWheelHorizontal(object sender, MouseEventArgs e)
        {
            if (Settings.HorizontalScroll)
            {
                Switch(e.Delta < 0 ? Direction.Left : Direction.Right);
            }
        }

        private void Switch(Direction direction, bool round = false)
        {
            if (!_switchInProgress)
            {
                _switchInProgress = true;
                var task = new Task(() =>
                {
                    var desktop = direction == Direction.Right
                        ? VirtualDesktop.Current.GetRight()
                        : VirtualDesktop.Current.GetLeft();

                    if (desktop != null)
                    {
                        desktop.Switch();
                    }
                    else
                    {
                        if (round)
                        {
                            var allDesktops = VirtualDesktop.GetDesktops();
                            if (direction == Direction.Right)
                            {
                                allDesktops.First().Switch();
                            }
                            else
                            {
                                allDesktops.Last().Switch();
                            }
                        }
                    }

                    Thread.Sleep(_delay);
                    _switchInProgress = false;
                });

                task.Start();
                //Task.WaitAll(task);
            }
        }
    }

    internal class ApplicationSettings : INotifyPropertyChanged
    {
        private readonly string _settingsPath = "SOFTWARE\\Arxsoft\\VDSwitch";
        private bool _autorun;
        private bool _capsLock;
        private bool _capsLockRound;
        private bool _horizontalScroll;

        private bool _settingsLoaded;
        private bool _shiftCapsLock;
        private bool _shiftCapsLockRound;

        public ApplicationSettings()
        {
            LoadSettings();
        }

        public bool HorizontalScroll
        {
            get { return _horizontalScroll; }
            set
            {
                if (value != _horizontalScroll)
                {
                    _horizontalScroll = value;
                    SaveSettings();
                    OnPropertyChanged();
                }
            }
        }

        public bool CapsLock
        {
            get { return _capsLock; }
            set
            {
                if (value != _capsLock)
                {
                    _capsLock = value;
                    SaveSettings();
                    OnPropertyChanged();
                }
            }
        }

        public bool CapsLockRound
        {
            get { return _capsLockRound; }
            set
            {
                if (value != _capsLockRound)
                {
                    _capsLockRound = value;
                    SaveSettings();
                    OnPropertyChanged();
                }
            }
        }

        public bool ShiftCapsLock
        {
            get { return _shiftCapsLock; }
            set
            {
                if (value != _shiftCapsLock)
                {
                    _shiftCapsLock = value;
                    SaveSettings();
                    OnPropertyChanged();
                }
            }
        }

        public bool ShiftCapsLockRound
        {
            get { return _shiftCapsLockRound; }
            set
            {
                if (value != _shiftCapsLockRound)
                {
                    _shiftCapsLockRound = value;
                    SaveSettings();
                    OnPropertyChanged();
                }
            }
        }

        public bool Autorun
        {
            get { return _autorun; }
            set
            {
                if (value != _autorun)
                {
                    _autorun = value;
                    SaveSettings();
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void LoadSettings()
        {
            _settingsLoaded = false;

            var isFirstRun = Registry.CurrentUser.OpenSubKey(_settingsPath) == null;
            if (isFirstRun)
            {
                SetDefault();
            }
            else
            {
                using (var key = Registry.CurrentUser.CreateSubKey(_settingsPath, true))
                {
                    Func<string, bool> readValue = s => (int) key.GetValue(s, 1) == 1;

                    HorizontalScroll = readValue(nameof(HorizontalScroll));
                    CapsLock = readValue(nameof(HorizontalScroll));
                    CapsLockRound = readValue(nameof(HorizontalScroll));
                    ShiftCapsLock = readValue(nameof(HorizontalScroll));
                    ShiftCapsLockRound = readValue(nameof(HorizontalScroll));
                }

                using (var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run"))
                {
                    Autorun = false;
                    var value = (string) key?.GetValue("VDSwitch", string.Empty);
                    if (!string.IsNullOrWhiteSpace(value) && value == $"\"{Application.ExecutablePath}\"")
                    {
                        Autorun = true;
                    }
                }
            }

            _settingsLoaded = true;
            SaveSettings();
        }

        private void SaveSettings()
        {
            if (_settingsLoaded)
            {
                using (var key = Registry.CurrentUser.CreateSubKey(_settingsPath, true))
                {
                    Action<string, bool> setValue =
                        (name, val) => key.SetValue(name, val ? 1 : 0, RegistryValueKind.DWord);

                    setValue(nameof(HorizontalScroll), HorizontalScroll);
                    setValue(nameof(CapsLock), CapsLock);
                    setValue(nameof(CapsLockRound), CapsLockRound);
                    setValue(nameof(ShiftCapsLock), ShiftCapsLock);
                    setValue(nameof(ShiftCapsLockRound), ShiftCapsLockRound);
                }

                var autoRunPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
                using (var key = Registry.CurrentUser.OpenSubKey(autoRunPath, true))
                {
                    key?.SetValue("VDSwitch", $"\"{Application.ExecutablePath}\"", RegistryValueKind.String);
                }
            }
        }

        public void SetDefault()
        {
            HorizontalScroll = true;
            CapsLock = true;
            CapsLockRound = true;
            ShiftCapsLock = true;
            ShiftCapsLockRound = true;
            Autorun = true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}